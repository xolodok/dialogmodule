<?php

namespace xolodok\dialog\controllers;

use Yii;
use yii\web\Response;
use xolodok\dialog\models\Message;
use yii\data\Pagination;
use xolodok\dialog\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class MessageController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Message();
        $model->loadDefaultValues();
        $model->user_id = Yii::$app->user->id;

        if($model->load(Yii::$app->request->post()) && $model->save()){

            $date = !Message::todayExists($model->dialog_id) ? date('d-m-Y') : false;
            $messageHtml = $this->renderPartial('_message', [
                'message' => $model,
                'date' => $date,
            ]);
            
            if(Module::getInstance()->messageAjax && Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;

                return [
                    'success' => true,
                    'message' => $messageHtml,
                ];
            }
        }

        return $this->redirect(['view', 'id' => $model->dialog_id]);
    }

    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
