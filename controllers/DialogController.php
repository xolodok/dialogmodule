<?php

namespace xolodok\dialog\controllers;

use Yii;
use yii\web\Response;
use xolodok\dialog\Module;
use xolodok\dialog\models\Dialog;
use xolodok\dialog\models\Message;
use xolodok\dialog\models\DialogForm;
use xolodok\dialog\models\MessageForm;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;

class DialogController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],            
        ];
    }

    /**
     * Displays dialogs index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $total = Dialog::getTotalByUser(Yii::$app->user->id);

        $pagination = new Pagination([
            'totalCount' => $total,
            'defaultPageSize' => Module::getInstance()->dialogPerPage,
            'forcePageParam' => false,
            'pageSizeParam' => false,
        ]);

        $dialogs = Dialog::findByUser(Yii::$app->user->id, $pagination->limit, $pagination->offset);

        return $this->render('index', [
            'dialogs' => $dialogs,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Creates a new Dialog model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $userId = Yii::$app->request->get('user_id');

        if(!$userId || !Module::getInstance()->userClass::exists($userId)){
            throw new NotFoundHttpException('User not found');            
        }

        if($userId == Yii::$app->user->id){
            throw new BadRequestHttpException('You can not send a message to yourself');
        }

        $model = new DialogForm();
        $model->userId = $userId;

        if($model->load(Yii::$app->request->post()) && $model->createDialog()){
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;

                return [
                    'success' => true,
                ];
            }
                     
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Dialog model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionCreateMessage()
    {
        $userId = Yii::$app->request->get('user_id');

        if(!$userId || !Module::getInstance()->userClass::exists($userId)){
            throw new NotFoundHttpException('User not found');            
        }

        $model = new MessageForm();
        $model->userId = $userId;
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        else if($model->load(Yii::$app->request->post()) && $model->createDialog()){                     
            return $this->redirect(['index']);
        }

        return $this->render('create_message', [
            'model' => $model,
        ]);
    }

    /**
     * Displays all messages from single dialogs.
     *
     * @return string
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');

        $dialog = $this->findModel($id);
        if(!$dialog->hasAccess()){
            throw new BadRequestHttpException('You do not have access to a dialogue!');
        }

        $dialog->readAllMessage(Yii::$app->user->id);

        $model = new Message();
        $model->loadDefaultValues();
        $model->dialog_id = $dialog->id;

        return $this->render('view', [
            'dialog' => $dialog,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dialog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param int $id
     *
     * @return mixed
     */    
    public function actionDelete($id)
    {
        $isDeleted = $this->findModel($id)->deleteByUser(Yii::$app->user->id);

        if(Module::getInstance()->deletedAjax && Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'success' => $isDeleted,
            ];
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dialog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
