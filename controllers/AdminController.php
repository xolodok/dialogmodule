<?php

namespace xolodok\dialog\controllers;

use Yii;
use xolodok\dialog\models\Dialog;
use xolodok\dialog\models\DialogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Response;
use xolodok\dialog\Module;

/**
 * AdminController implements the CRUD actions for Dialog model.
 */
class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],            
        ];
    }

    /**
     * Lists all Dialog models.
     * @return mixed
     */
    public function actionIndex($userId = null)
    {
        $searchModel = new DialogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $user = Module::getInstance()->userClass::findIdentity($userId);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }

    /**
     * Displays a single Dialog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $userId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        $html = $this->renderPartial('view', [
            'model' => $model,
            'userId' => $userId,
        ]);

        return ['success' => true, 'html' => $html, 'title' => $model->subject];
    }

    /**
     * Finds the Dialog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dialog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dialog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
