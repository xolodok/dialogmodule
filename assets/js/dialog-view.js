$('.view-message').on('click', function(){
    $.ajax({
        url: $(this).data('url'),
        success: function(result){
            $('#message-container').html(result.html);
            $('.modal-title').text(result.title);
            $('#messageModal').modal('show');
        },
    });
});