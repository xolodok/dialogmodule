<?php

namespace xolodok\dialog;

use Yii;
use xolodok\dialog\interfaces\IDialog;
use yii\base\InvalidConfigException;

/**
 * dialog module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'xolodok\dialog\controllers';

    /**
     * @var User model class
     */
    public $userClass = 'common\models\User';

    /**
     * @var Number of dialogs per page
     */
    public $dialogPerPage = 20;

    /**
     * @var Send messages by ajax
     */
    public $messageAjax = true;

    /**
     * @var Deleted dialogs by ajax
     */
    public $deletedAjax = true;

    /**
     * @var User captcha for unrefister user
     */
    public $captcha = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!isset(class_implements($this->userClass)['xolodok\dialog\interfaces\IDialog'])) {
            throw new InvalidConfigException('The "userClass" must implements IDialog.');
        }

        $this->registerTranslations();    
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['xolodok/dialog/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/xolodok/yii2-dialog/messages',
            'fileMap' => [
                'xolodok/dialog/dialog' => 'default.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('xolodok/dialog/' . $category, $message, $params, $language);
    }    
}
