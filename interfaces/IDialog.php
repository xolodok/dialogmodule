<?php

namespace xolodok\dialog\interfaces;

interface IDialog
{
    /**
     * Этот метод возвращает путь к аватарке пользователя
     * @return string
     */
	public function getAvatar();

    /**
     * Этот метод возвращает имя пользователя
     * @return string
     */
	public function getName();

    /**
     * Этот метод проверяет пользователя на существования
     * @return bool
     */
    public static function exists($userId);    
}
