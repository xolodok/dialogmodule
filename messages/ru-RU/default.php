<?php

return [
	'ID' => 'ID',
	'Subject' => 'Тема',
	'Username' => 'Имя',
	'Email' => 'Email',
	'Sender' => 'Отправитель',
	'Date' => 'Дата отправки',
	'Read' => 'Прочитано',

    'Messages for user "{fullName}"' => 'Сообщения пользователя "{fullName}"',
    'Dialogs' => 'Диалоги',
    'Interlocutor' => 'Собеседник',
    'View message history' => 'Просмотреть историю сообщений',
    'Close' => 'Закрыть',

    'Dialogs' => 'Диалоги',
    'Delete' => 'Удалить',
    'Create new message' => 'Создать новое сообщение',
    'Dialogs is Empty' => 'Нет диалогов',
    'Messages' => 'Сообщения',
];