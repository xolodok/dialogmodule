<?php

use yii\db\Migration;

class m171024_063133_dialog_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('dialogs', [
            'id' => $this->primaryKey(),
            'subject' => $this->string(),
            'sender_id' => $this->integer()->notNull()->defaultValue(0),
            'recipient_id' => $this->integer()->notNull(),
            'sender_delete' => $this->smallInteger()->notNull()->defaultValue(0),
            'recipient_delete' => $this->smallInteger()->notNull()->defaultValue(0),
            'username' => $this->string(),
            'email' => $this->string(),
        ]);

        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'dialog_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull()->defaultValue(0),
            'text' => $this->text(),
            'read' => $this->smallInteger()->notNull()->defaultValue(0),
            'date' => $this->timestamp(),
        ]);

        $this->addForeignKey('{{%fk_dialog_message}}', '{{%messages}}', 'dialog_id', '{{%dialogs}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable('dialogs');
        $this->dropTable('messages');
    }
}
