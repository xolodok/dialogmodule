Dialog
======
Dialog

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist xolodok/yii2-dialog "*"
```

or add

```
"xolodok/yii2-dialog": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \xolodok\dialog\AutoloadExample::widget(); ?>```