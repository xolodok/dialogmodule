<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
?>
<div class="dialog-form">
	<h1>Create new message</h1>
	
	<?php $form = ActiveForm::begin()?>

	<?= $form->field($model, 'subject')?>

	<?= $form->field($model, 'text')->textarea()?>

	<?= $form->field($model, 'userId')->hiddenInput()->label(false)?>

	<?= Html::submitButton('Send', ['class' => 'btn btn-success btn-block'])?>

	<?php ActiveForm::end()?>

</div>
