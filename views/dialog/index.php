<?php
use yii\helpers\Url;
use yii\helpers\Html;
use xolodok\dialog\AssetBundle;
use xolodok\dialog\Module;

/* @var $this yii\web\View */
AssetBundle::register($this);
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?= Module::t('dialog', 'Dialogs')?></h3>
				</div>
				<div class="panel-body">

					<?php if(!empty($dialogs)):?>

						<ul class="media-list">

							<?php foreach($dialogs as $dialog):?>

								<li class="media">
									<a href="<?= Url::to(['delete', 'id' => $dialog->id])?>" data-action="delete" data-method="post">
										<button type="button" class="close">
											<span aria-hidden="true">&times;</span>
											<span class="sr-only"><?= Module::t('dialog', 'Delete')?></span>
										</button>									
									</a>
									<a href="<?= Url::to(['view', 'id' => $dialog->id])?>">
										<div class="media-left">

											<?php 
												if($dialog->isGuest()){
													echo \cebe\gravatar\Gravatar::widget([
													    'email' => $dialog->email,
													    'options' => [
													        'alt' => $dialog->username
													    ],
													    'size' => 64
													]);
												}
												else{
													echo Html::img($dialog->getInterlocutor(Yii::$app->user->id)->getAvatar());
												}
											?>
											
										</div>
										<div class="media-body">
											<h4 class="list-group-item-heading">
												<?= $dialog->subject?>
												<span class="pull-right"><?= $dialog->getSenderName(Yii::$app->user->id)?></span>
											</h4>
											<p class="list-group-item-text">
												<?= $dialog->message->text?>
												<span class="pull-right"><?= $dialog->message->formattedDate?></span>
											</p>
										</div>
									</a>
								</li>

							<?php endforeach?>

						</ul>

					<?php else:?>

						<h2>Dialog is Empty</h2>

					<?php endif?>

				</div>
			</div>
		</div>
	</div>
</div>

<?php
	if(Module::getInstance()->deletedAjax){
		$this->registerJs("
			$('body').on('click', '[data-action=\"delete\"]', function(){
				var dialogContainer = $(this).parent();
				$.ajax({
					type: 'post',
					url: $(this).attr('href'),
					success: function(result){
						if(result.success){
							dialogContainer.fadeOut(500, function(){
								dialogContainer.remove();
							});
						}
					},
				});

				return false;
			});
		");
	}
?>
