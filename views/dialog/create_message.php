<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use xolodok\dialog\Module;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
?>
<div class="dialog-form">
	<h1><?= Module::t('dialog', 'Create new message')?></h1>
	
	<?php $form = ActiveForm::begin()?>

	<?= $form->field($model, 'username')?>

	<?= $form->field($model, 'email')?>

	<?= $form->field($model, 'subject')?>

	<?= $form->field($model, 'text')->textarea()?>

	<?php $form->field($model, 'captcha', ['enableAjaxValidation' => true, 'enableClientValidation' => false])->widget(Captcha::className(), [
		'captchaAction' => 'captcha',
    ]) ?>

	<?= $form->field($model, 'reCaptcha')->widget(
	    \himiklab\yii2\recaptcha\ReCaptcha::className(),
	    ['siteKey' => '6LfyfzgUAAAAAGR2PD9tK-imTGNwQrQJka0wdyGO']
	) ?>
		
	<?= $form->field($model, 'userId')->hiddenInput()->label(false)?>

	<?= Html::submitButton(Module::t('dialog', 'Send'), ['class' => 'btn btn-success btn-block'])?>

	<?php ActiveForm::end()?>

</div>
