<?php
use xolodok\dialog\AssetBundle;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use xolodok\dialog\widgets\MessageWidget;

AssetBundle::register($this);
?>

<div class="container-fluid">
	<div class="row">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><?= $dialog->subject?></h3>
			</div>
			<div class="panel-body">

				<div id="message-container">

				<?php 
					foreach($dialog->sortMessageByDate as $date => $messages){
						foreach ($messages as $key => $message) {
							echo $this->render('/message/_message', [
								'message' => $message,
								'date' => !$key ? $date : false,
							]);
						}
					}
				?>

				</div>

				<?php if(!$dialog->isGuest()):?>

					<div class="message-form">

						<?= MessageWidget::widget([
							'messageContainer' => '#message-container',
							'dialogId' => $dialog->id,
						])?>

					</div>

				<?php endif?>

			</div>
		</div>		
	</div>
</div>
