<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\dialog\models\Dialog */

$this->title = Yii::t('main', 'Update {modelClass}: ', [
    'modelClass' => 'Dialog',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Dialogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="dialog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
