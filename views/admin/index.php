<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use xolodok\dialog\AssetBundle;
use xolodok\dialog\Module;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\dialog\models\DialogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('dialog', 'Messages for user "{fullName}"', [
    'fullName' => $user->profile->fullName,
]);
$this->params['breadcrumbs'][] = $this->title;

AssetBundle::register($this);
?>
<div class="dialog-index">

    <?= GridView::widget([
        'bordered' => true,
        'hover' => true,
        'responsive'=>true,
        'panel'=>[
            'type' => GridView::TYPE_INFO,
            'heading' => Module::t('dialog', 'Dialogs'),
        ],
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-centered'
        ],        
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'subject',
                'enableSorting' => false,
            ],
            [
                'label' => Module::t('dialog', 'Interlocutor'),
                'value' => function ($model, $index, $widget) use ($user) {
                    return $model->getSenderName($user->id);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) use ($user) {
                        return Html::button(Module::t('dialog', 'View message history'), [
                            'class' => 'btn btn-success view-message',
                            'data-url' => Url::to(['/dialog/admin/view', 'id' => $model->id, 'userId' => $user->id]),
                        ]);
                    }
                ],
            ],
        ],        
    ]); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= Module::t('dialog', 'Close')?></span></button>
                <h4 class="modal-title" id="myModalLabel"> </h4>
            </div>
            <div class="modal-body">
                <div id="message-container"> </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs("
    $('.view-message').on('click', function(event){
        $.ajax({
            url: $(this).data('url'),
            success: function(result){
                $('#message-container').html(result.html);
                $('.modal-title').text(result.title);
                $('#messageModal').modal('show');
            },
        });
    })
")?>