<div class="container">

    <?php foreach($model->messages as $key => $message):?>
<!--         <p class="datestamp">May 20, 2014, 4:16 PM</p>
 -->
        <div class="col-md-10">
            <div class="bubble<?= $message->isSender($userId) ? ' bubble-alt green' : ''?>">
                <p><?= $message->text?></p>
            </div>
        </div>
        <div class="col-md-2">
            <p class="message-time"><?= Yii::$app->formatter->asDate($message->date, 'php:d-m-Y H:i')?></p>
        </div>

    <?php endforeach?>
                
</div>
