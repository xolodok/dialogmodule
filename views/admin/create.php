<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\dialog\models\Dialog */

$this->title = Yii::t('main', 'Create Dialog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Dialogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dialog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
