<?php

use yii\helpers\Html;
?>

<?php if($date !== false) echo Html::tag('h4', $date);?>

<div class="col-md-10">
	<div class="bubble<?= $message->isSender(Yii::$app->user->id) ? ' bubble-alt green' : ''?>">
		<p><?= $message->text?></p>
	</div>
</div>
<div class="col-md-2">
	<p class="message-time"><?= $message->formattedDate?></p>
	<p class="message-time"><?= $message->isRead() ? 'Read' : 'No read'?></p>
</div>
