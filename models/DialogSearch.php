<?php

namespace xolodok\dialog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use xolodok\dialog\models\Dialog;

/**
 * DialogSearch represents the model behind the search form about `common\modules\dialog\models\Dialog`.
 */
class DialogSearch extends Dialog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sender_id', 'recipient_id', 'sender_delete', 'recipient_delete'], 'integer'],
            [['subject', 'username', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dialog::find()
            ->where(['sender_id' => $params['userId']])
            ->orWhere(['recipient_id' => $params['userId']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sender_id' => $this->sender_id,
            'recipient_id' => $this->recipient_id,
            'sender_delete' => $this->sender_delete,
            'recipient_delete' => $this->recipient_delete,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
