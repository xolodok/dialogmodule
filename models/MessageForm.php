<?php

namespace xolodok\dialog\models;

use Yii;
use yii\base\Model;

class MessageForm extends Model
{
	public $subject;

	public $text;

	public $username;

	public $email;

	public $userId;

	public $captcha;

	public $reCaptcha;

	private $_dialog;

	public function rules()
	{
		return [
			[['subject', 'text', 'email', 'username', 'reCaptcha'], 'required'],
			['email', 'email'],
			[['subject', 'username'], 'string', 'max' => 255],
			['text', 'string'],
			['userId', 'integer'],
			//['captcha', 'captcha', 'captchaAction'=>'dialog/dialog/captcha'],
            ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LfyfzgUAAAAAItJByhurTXM71c9m5zY7eYnIK9i', 'uncheckedMessage' => 'Please confirm that you are not a bot.'],			
		];
	}

	public function createDialog()
	{
		if($this->validate()){
			$this->_dialog = new Dialog();
			$this->_dialog->loadDefaultValues();
			$this->_dialog->setAttributes([
				'email' => $this->email,
				'username' => $this->username,
				'subject' => $this->subject,
				'recipient_id' => $this->userId,
			]);

			if($this->_dialog->save(false)){
				$message = new Message();
				$message->loadDefaultValues();
				$message->text = $this->text;
				$message->link('dialog', $this->_dialog);
			}

			return true;
		}

		return false;
	}
}