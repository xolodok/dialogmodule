<?php

namespace xolodok\dialog\models;

use Yii;
use yii\base\Model;

class DialogForm extends Model
{
	public $subject;

	public $text;

	public $userId;

	private $_dialog;

	public function rules()
	{
		return [
			[['subject', 'text'], 'required'],
			['subject', 'string', 'max' => 255],
			['text', 'string'],
			['userId', 'integer'],
		];
	}

	public function createDialog()
	{
		if($this->validate()){
			$this->_dialog = new Dialog();
			$this->_dialog->sender_id = Yii::$app->user->id;
			$this->_dialog->recipient_id = $this->userId;
			$this->_dialog->subject = $this->subject;

			if($this->_dialog->save()){
				$message = new Message();
				$message->loadDefaultValues();
				$message->text = $this->text;
				$message->user_id = Yii::$app->user->id;
				$message->link('dialog', $this->_dialog);
			}

			return true;
		}

		return false;
	}
}