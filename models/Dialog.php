<?php

namespace xolodok\dialog\models;

use Yii;
use xolodok\dialog\Module;
use xolodok\dialog\models\query\DialogQuery;

/**
 * This is the model class for table "dialogs".
 *
 * @property integer $id
 * @property string $subject
 * @property integer $sender_delete
 * @property integer $recipient_delete
 * @property string $username
 * @property string $email
 */
class Dialog extends \yii\db\ActiveRecord
{
    /**
     * Deleted dialogs flag
     */
    const DELETE = 1;

    /**
     * Not deleted dialogs flag
     */
    const NOT_DELETE = 0;

    /**
     * Unregister user
     */
    const GUEST = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dialogs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_delete', 'recipient_delete', 'recipient_id', 'sender_id'], 'integer'],
            [['subject', 'username', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('dialog', 'ID'),
            'subject' => Module::t('dialog', 'Subject'),
            'sender_delete' => Module::t('dialog', 'Sender Delete'),
            'recipient_delete' => Module::t('dialog', 'Recipient Delete'),
            'username' => Module::t('dialog', 'Username'),
            'email' => Module::t('dialog', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['dialog_id' => 'id'])
            ->with('user')
            ->orderBy('date ASC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['dialog_id' => 'id'])
            ->orderBy('date DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(Module::getInstance()->userClass, ['id' => 'sender_id']);
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(Module::getInstance()->userClass, ['id' => 'recipient_id']);
    }

    /**
     * Определяет второго собеседника
     * @param int $userId
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutor($userId)
    {
        if($this->isGuest()){
            return null;
        }

        return $userId == $this->recipient_id ? $this->sender : $this->recipient;
    }

    /**
     * Возвращает имя второго юзера(собеседника) в диалоге
     * @param int $userId Id пользователя
     * @return string
     */
    public function getSenderName($userId)
    {
        if($this->isGuest()){
            return $this->username;
        }

        return $this->getInterlocutor($userId)->name;
    }

    /**
     * Проверка, является ли отправитель зарегистрированным юзером
     * @return bool
     */
    public function isGuest()
    {
        return $this->sender_id === self::GUEST;
    }

    /**
     * @return DialogQuery
     */
    public static function find()
    {
        return \Yii::createObject(DialogQuery::className(), [get_called_class()]);
    }

    /**
     * Возвращает количество активных диалогов пользователя
     * @param int $userId Id пользователя
     * @return int
     */
    public static function getTotalByUser($userId)
    {
        return static::find()
            ->byUser($userId)
            ->notDelete($userId)
            ->count();
    }

    /**
     * Возвращает диалоги пользователя
     * @param int $userId Id пользователя
     * @param int $limit Лимит записей
     * @param int $offset Пропуск записей
     * @return array Dialog[]
     */
    public static function findByUser($userId, $limit = false, $offset = false)
    {
        $query = static::find()
            ->with('message')
            ->byUser($userId)
            ->notDelete($userId);

        if($limit !== false){
            $query->limit($limit);
        }

        if($offset !== false){
            $query->offset($offset);
        }

        return $query->all();
    }

    /**
     * Отмечает все сообщения диалога как прочитанные
     * @param int $userId Id пользователя
     * @return void
     */
    public function readAllMessage($userId)
    {
        Message::updateAll(['read' => Message::READ], [
            'dialog_id' => $this->id, 
            'user_id' => $userId == $this->recipient_id ? $this->sender_id : $this->recipient_id,
        ]);
    }

    public function deleteByUser($userId)
    {
        $attribute = $userId === $this->sender_id ? 'sender_delete' : 'recipient_delete';
        return (bool) $this->updateAttributes([$attribute => self::DELETE]);
    }

    public function hasAccess()
    {
        return Yii::$app->user->id === $this->sender_id || Yii::$app->user->id === $this->recipient_id;
    }

    public function getSortMessageByDate()
    {
        $messages = [];

        foreach ($this->messages as $message) {
            if(!isset($messages[$message->dateCreated])){
                $messages[$message->dateCreated] = [];
            }

            $messages[$message->dateCreated][] = $message;
        }

        return $messages;
    }

    /**
     * Возвращает id второго собеседника
     * @param int $userId
     * @return int
     */
    public function getInterlocutorId($userId)
    {
        if($this->isGuest()){
            return null;
        }

        return $userId == $this->recipient_id ? $this->sender_id : $this->recipient_id;
    }    
}
