<?php

namespace xolodok\dialog\models\query;

use yii\db\ActiveQuery;
use xolodok\dialog\models\Dialog;

class DialogQuery extends ActiveQuery
{
    public function byUser($userId)
    {
        return $this->andWhere(['or', ['sender_id' => $userId], ['recipient_id' => $userId]]);
    }

    public function notDelete($userId)
    {
        return $this->andWhere("IF(`sender_id` = {$userId}, `sender_delete`, `recipient_delete`) != " . Dialog::DELETE);
    }
}
