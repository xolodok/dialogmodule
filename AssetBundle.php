<?php

namespace xolodok\dialog;

/**
 * Class AssetBundle
 * @package xolodok\dialog
 */
class AssetBundle extends \yii\web\AssetBundle
{
    /**
     * @var array
     */
    public $css = [
        'css/style.css',
    ];

    /**
     * @inherit
     */
    // public $js = [
    //     'js/dialog-views.js',
    // ];

    /**
     * @var array
     */
    public $depends = array(
        'yii\web\JqueryAsset'
    );

    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}