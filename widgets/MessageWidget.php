<?php

namespace xolodok\dialog\widgets;

use Yii;
use xolodok\dialog\Module;
use yii\base\InvalidConfigException;
use xolodok\dialog\models\Message;

class MessageWidget extends \yii\base\Widget
{
    public $viewFile = 'message-form';

    public $messageContainer;

    public $dialogId;

    public function init()
    {
        parent::init();

        if($this->dialogId === null){
            throw new InvalidConfigException('The "dialogId" property must be set.');
        }

        if(Module::getInstance()->messageAjax && $this->messageContainer === null){
            throw new InvalidConfigException('The "messageContainer" property must be set.');            
        }
    }

    public function run()
    {
        $model = new Message();
        $model->loadDefaultValues();
        $model->user_id = Yii::$app->user->id;
        $model->dialog_id = $this->dialogId;

        return $this->render($this->viewFile, [
            'model' => $model,
            'messageContainer' => $this->messageContainer,
        ]);
    }
}