<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use xolodok\dialog\Module;
?>

	<?php $form = ActiveForm::begin([
		'id' => 'message-form',
		'action' => Url::to(['/dialog/message/create']),
		'validateOnBlur' => false,
		'validateOnType' => false,
		'validateOnChange' => false,
		'validateOnSubmit' => true,						
	])?>

	<?= $form->field($model, 'text')->textarea(['rows' => 3])->label(false)?>

	<?= $form->field($model, 'dialog_id')->hiddenInput()->label(false)?>

	<?= Html::submitButton(Module::t('dialog', 'Send'), ['class' => 'btn btn-success btn-block'])?>

	<?php ActiveForm::end()?>

<?php
	if(Module::getInstance()->messageAjax){
		$this->registerJs("
			$('#message-form').on('beforeSubmit', function(event){
				$.ajax({
					type: 'post',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(result){
						if(result.success){
							$('" . $messageContainer . "').append(result.message);
							$(event.currentTarget).trigger('reset');
						}
					},
					error: function(){
						
					}
				});
				return false;
			})		
		");		
	}
?>
